--// Effects Factory.
--// Provides an API to add some spice to visuals - snow, rain, etc. Shader effects, if ever implemented, would also be here.

local class = require "libs/hump/class"
local timer = require "libs/hump/timer"

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc = game.cx, game.cy, game.xc

local tremove, mfloor, msin, lmrand = table.remove, math.floor, math.sin, love.math.random

fxfactory = class {}

function fxfactory:init()
	self.alpha = 255
	self.timer = timer.new()
	self.mode = 0				--// 0 - disabled, 1 - snow, 2 - snow (stopping)
	self.drawable = {}
	
	self.snow = {}	
	for i = 1, 8, 1 do
		self.snow[i] = love.graphics.newImage("graphics/static/snow"..i..".png")
	end
	self.snow_timer = 0
	self.snow_timer_slow = 0
	self.snow_timestamp = 0
	self.snow_threshold = 0.5
	
end

function fxfactory:update(dt)
	self.timer:update(dt)
	self.snow_timer = self.snow_timer + dt
	self.snow_timer_slow = mfloor(self.snow_timer * 10) / 10
	local switch = {
		[0] = function()	end,
		[1] = function()
			
			local function addflake()
				if lmrand() < self.snow_threshold then
					self.drawable[#self.drawable + 1] = {
						flakeimg = lmrand(1, 8),
						rotvec = lmrand(-15, 15),
						rotation = 0,
						size = ((1 + lmrand()) / 20) * sc,
						y = -50 * sc,
						x = lmrand(cx, xc),
						multiplier = lmrand() * 5
					}
				end
			end
		
			for i = #self.drawable, 1, -1 do
				self.drawable[i].y = self.drawable[i].y + (((self.drawable[i].size * 500) ^ 1.2) * dt)
				self.drawable[i].x = self.drawable[i].x + (lmrand(0, 40) * dt)
				self.drawable[i].rotation = self.drawable[i].rotation + (self.drawable[i].rotvec * dt * 0.1)
				if self.drawable[i].x > xc then
					self.drawable[i].x = -40
				end
				if self.drawable[i].y > sH then
					tremove(self.drawable, i)
				end
			end
			if #self.drawable < 500 and self.snow_timer_slow % 0.1 == 0 and self.snow_timestamp ~= self.snow_timer_slow then
				self.snow_timestamp = self.snow_timer_slow
				for i = 1, 5, 1 do
					addflake()
				end
			end
			self.snow_threshold = mfloor((love.math.noise(self.snow_timer / 8) * 100) + 0.5) / 100
		end,
		[2] = function()
			for i = #self.drawable, 1, -1 do
				self.drawable[i].y = self.drawable[i].y + (((self.drawable[i].size * 500) ^ 1.2) * dt)
				self.drawable[i].x = self.drawable[i].x + (lmrand(0, 40) * dt)
				self.drawable[i].rotation = self.drawable[i].rotation + (self.drawable[i].rotvec * dt * 0.1)
				if self.drawable[i].x > xc then
					self.drawable[i].x = -40
				end
				if self.drawable[i].y > sH then
					tremove(self.drawable, i)
				end
			end
			if #self.drawable == 0 then
				self.mode = 0
			end
			self.snow_threshold = mfloor((love.math.noise(self.snow_timer / 8) * 100) + 0.5) / 100
		end,
	}
	
	switch[self.mode]()
end

function fxfactory:draw()
	love.graphics.setColor(255, 255, 255, self.alpha)
	local switch = {
		[0] = function()	end,
		[1] = function()
			for x = 1, #self.drawable do
				love.graphics.draw(self.snow[self.drawable[x].flakeimg], self.drawable[x].x + (msin(self.snow_timer * self.drawable[x].multiplier) * (50 - (self.drawable[x].multiplier * 9))) * (self.drawable[x].size * 10), self.drawable[x].y, self.drawable[x].rotation, self.drawable[x].size, self.drawable[x].size, self.snow[self.drawable[x].flakeimg]:getWidth() / 2, self.snow[self.drawable[x].flakeimg]:getHeight() / 2)
			end
		end,
		[2] = function()
			for x = 1, #self.drawable do
				love.graphics.draw(self.snow[self.drawable[x].flakeimg], self.drawable[x].x + (msin(self.snow_timer * self.drawable[x].multiplier) * (50 - (self.drawable[x].multiplier * 9))) * (self.drawable[x].size * 10), self.drawable[x].y, self.drawable[x].rotation, self.drawable[x].size, self.drawable[x].size, self.snow[self.drawable[x].flakeimg]:getWidth() / 2, self.snow[self.drawable[x].flakeimg]:getHeight() / 2)
			end
		end,
	}
	switch[self.mode]()
	love.graphics.setColor(255, 255, 255, 255)
end

function fxfactory:setMode(m)
	local switch = {
		[0] = function()
			self.drawable = {}
			self.snow_timer = 0
			self.snow_timer_slow = 0
			self.snow_timestamp = 0
			self.mode = 0
		end,
		
		[1] = function()
			self.mode = 1
		end,
		[2] = function()
			self.mode = 2
		end,
	}
	switch[m]()
end

function fxfactory:setAlpha(m)
	local switch = {
		[0] = function()	self.alpha = 0	end,
		[1] = function()	self.alpha = 1	end,
	}
	switch[m]()
end