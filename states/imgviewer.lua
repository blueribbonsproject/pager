local timer = require("libs/hump/timer")
local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS
local imgviewer = {}

function imgviewer:init()
	self.sfx = game.sfx	
	self.timer = timer.new()
	self.activeColor = game.activeColor
end

function imgviewer:enter(state, img)

	about.scene = "imgviewer"
	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end
	
	self.quit = function()
		TEsound.play(self.sfx[1], "click", 0.6)	
		GS.switch(require("states/gallery"))
	end
	
	gooi.newButton("menu_btn", _, xc - (64 * sc + 12 * sc), 12 * sc, 64 * sc, 64 * sc, "graphics/static/gear.png", "menu", sc)
	gooi.get("menu_btn"):onRelease(function() self.quit() end)
	
	self.image = love.graphics.newImage(img)
	self.width, self.height = self.image:getWidth(), self.image:getHeight()
	
end

function imgviewer:leave()
	gooi.remove("menu_btn")
end

function imgviewer:update(dt)
	
	gooi.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end
		
	end
	
	self.timer:update(dt)
	
end

function imgviewer:draw()
	love.graphics.draw(self.image, self.width / 2, self.height / 2, 0, sc, sc, self.width / 2, self.height / 2)
	gooi.draw("menu")	
	drawFPS()
end

function imgviewer:keypressed(key, unicode)	
	self.quit()
end

function imgviewer:joystickpressed(joystick, button)
	self.quit()
end

return imgviewer