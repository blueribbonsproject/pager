﻿local timer = require("libs/hump/timer")
local lang = {}

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS

function lang:init()
	self.sfx = game.sfx
	self.uiColor = game.uiColor
	self.activeColor = game.activeColor
end

function lang:enter(state)
	
	about.scene = "lang"
	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end	
	
	self.menu = { labels = { [1] = "English", [2] = "Русский" }, active = 1 }
	self.menu.langs = { [1] = "en", [2] = "ru" }
	
	gooi.newPanel("lang-grid", cx + 260 * sc, 250 * sc, (xc - cx) - 520 * sc, sH - 500 * sc, "grid 2x1")
	for i = 1, #self.menu.labels, 1 do
		gooi.get("lang-grid"):add(
			gooi.newButton("lang-"..self.menu.langs[i], self.menu.labels[i], _, _, _, _, "graphics/static/"..self.menu.langs[i]..".png", _, sc)
		)
		gooi.get("lang-"..self.menu.langs[i]):onRelease(function()	about.settings.lang = self.menu.langs[i]		GS.switch(require("states/sound"))	end)
		gooi.get("lang-"..self.menu.langs[i]).borderWidth = 5 * sc
	end
	
	self.onKeyDown = function()	
		gooi.get("lang-"..self.menu.langs[self.menu.active]).bgColor = game.style.bgColor		
		if self.menu.active + 1 <= #self.menu.langs then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gooi.get("lang-"..self.menu.langs[self.menu.active]).bgColor = game.style.bgColor		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.langs		
		end	
	end
	
	self.onKeyUse = function()
		TEsound.play(self.sfx[1], "click", 0.6)
		about.settings.lang = self.menu.langs[self.menu.active]
		GS.switch(require("states/sound"))
	end
	
	self.timer = timer.new()
	self.joystickBlocked = false
	
end

function lang:leave()	
	self.timer:clear()
	gooi.remove("lang-ru")
	gooi.remove("lang-en")	
end

function lang:update(dt)
	gooi.get("lang-"..self.menu.langs[self.menu.active]).bgColor = self.activeColor
	gooi.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end
		
	end
	self.timer:update(dt)	
end

function lang:draw()	
	gooi.draw()
	drawFPS()	
end

function lang:keypressed(key, unicode)
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.onKeyUse()	end,
		[" "] = function()	self.onKeyUse()	end,
		["e"] = function()	self.onKeyUse()	end,
		["x"] = function()	self.onKeyUse()	end,
	}
	if switch[key] then switch[key]()	end
end

function lang:joystickpressed(joystick, button)	
	if	button == 5 or  button == 6 or button == 7	then	TEsound.play(self.sfx[2], "switch", 0.6)		end
	local switch = {
		[1] = function()	self.onKeyUse()	end,
		[2] = function()	self.onKeyUse()	end,
		[3] = function()	self.onKeyUse()	end,
		[4] = function()	self.onKeyUse()	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyUp()	end,
		[8] = function()	self.onKeyUse()	end,
	--	[11] = function()	self.onKeyUse()	end
	}
	if switch[button] then switch[button]()	end	
end
return lang