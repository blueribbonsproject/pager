local timer = require("libs/hump/timer")
local json = require("libs/dkjson")
local intro = {}
local sW, sH, sc = game.sW, game.sH, game.sc

function intro:init()
	self.timer = timer.new()
end

function intro:enter(state)
	
	about.scene = "intro"
	
	self.logo1 = love.graphics.newImage("graphics/static/logo_liminalia.png")
	self.logo2 = love.graphics.newImage("graphics/static/logo_sae.png")
	self.target1 = { opacity = 0 }
	self.target2 = { y = sH / 2 }
	self.target3 = { y = sH + self.logo2:getHeight() / 2 }
	
	self.timer:tween(2, self.target1, {opacity = 255}, "linear", function()
		TEsound.play(game.sfx[4], "sfx4", 0.6)
		self.timer:tween(0.5, self.target2, { y = 0 - self.logo1:getHeight() / 2 }, "linear")
		self.timer:tween(0.5, self.target3, { y = sH / 2 }, "linear", function ()		
			self.timer:add(1, function()
				self.timer:tween(1.5, self.target1, {opacity = 0}, "linear", function()			
					if love.filesystem.exists("brm-settings.cfg") then
						local data = love.filesystem.read("brm-settings.cfg")
						local config = json.decode(data)
						about.settings = config.settings
						GS.switch(require("states/mainmenu"))
					else
						if web == true then
							GS.switch(select_state["sound"])
						else
							GS.switch(require("states/lang"))
						end
					end
				end)
			end)
		end)
	end)
	
end

function intro:leave()
	self.timer:clear()
end

function intro:update(dt)
	
	self.timer:update(dt)
	
end

function intro:draw()
	love.graphics.setColor({ 255, 255, 255, self.target1.opacity })
	love.graphics.draw(self.logo1, sW / 2, self.target2.y, 0, sc, sc, self.logo1:getWidth() / 2, self.logo1:getHeight() / 2)
	love.graphics.draw(self.logo2, sW / 2, self.target3.y, 0, sc, sc, self.logo2:getWidth() / 2, self.logo2:getHeight() / 2)
	love.graphics.setColor({ 255, 255, 255, 255 })	
end
return intro