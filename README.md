# Pager
A simple LÖVE-powered VN engine.
A platform-independent solution which runs on all platforms supported by LÖVE and on a browser page using a web player (https://github.com/TannerRogalsky/punchdrunk).

# Current version
1.3