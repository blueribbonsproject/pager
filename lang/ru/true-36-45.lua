local src = {}
src[1] = '"Это и вправду хорошая идея, Чиё-тян," - говорит Нозаки, гладя её по голове, - "но я думаю, что это немного нечестно по отношению к читателям. Кроме того, приравнивать всё, что случилось, всего лишь ко сну - слишком грустно."'
src[2] = '"Я в туалет буквально на пару секунд."\nНозаки встает и уходит.'
src[3] = '"Альтернативной меня, с бантиками другого цвета, просто не может быть. Максимум, это могу быть я, надев другую пару.'
src[4] = 'Невозможно, правда?" - Чиё невинно щебечет сама с собой.'
src[5] = '"Интересно, какого цвета бантики Нозаки хотел бы, чтобы я носила?"\nЧиё покраснела, потупив взгляд.'
src[6] = '"Почему Нозаки так долго не возвращается?"'
return src
