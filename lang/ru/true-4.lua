local src = {}
src[1] = '"Хаха...ха, и как он только мог сюда попасть? Его больше нет.'
src[2] = 'Так могу ли я зайте к тебе сегодня, Нозаки-кун?"'
src[3] = "Chiyo, I have a secret. I'm not the original Nozaki-kun, I'm just someone who's inserting onto Nozaki-kun. I'm not really Nozaki-kun, I'm just pretending to be him. Don't you get it Chiyo? I couldn't bear to hide it any longer. The real Nozaki-kun is long gone, locked up producing manga all day and night."
src[4] = "Чиё, а у тебя случайно нет сестры-близнеца?"
src[5] = "Это призрак?!"
return src