local src = require("lang/"..about.settings.lang.."/true-13")
script = {
	[1] = { "set_page", { name = "Dark Alley 12", image = "cg-1/12" } },
	[2] = { "fade_text", { dir = "in", time = 1 } },
	[3] = { "fill", {time = 4, mode = "in" } },
	[4] = { "set_page", { name = "Dark Alley 13", image = "cg-1/13" }, { mode = "fade", time = 0.5 } } ,
	[5] = { "fill", { time = 3, mode = "out" } },
	[6] = { "fade_text", { dir = "out", time = 1 } },
	[7] = { "print", { msg = src[1], mode = 0 } },
	[8] = { "input", { type = 0 } },
	[9] = { "fade_text", { dir = "in", time = 1 } },
	[10] = { "clear" },
	[11] = { "fade_page", { dir = "in", time = 3 } },
	[12] = { "fade_text", { dir = "out", time = 1 } },
	[13] = { "print", { msg = src[2], mode = 0 } },
	[14] = { "input", { type = 0 } },
	[15] = { "fade_text", { dir = "in", time = 1 } },
	[16] = { "fill", { mode = "in", instant = true } },
	[17] = { "read", { name = "true-14-19" } },	
}
return script