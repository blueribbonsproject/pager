local src = require("lang/"..about.settings.lang.."/true-2-2")
script = {
	[1] = { "set_page", { name = "Dark Alley 1", image = "cg-1/1" } },
	[2] = { "set_page", { name = "Dark Alley 2", image = "cg-1/2" }, { mode = "fade", time = 0.2 } } ,
	[3] = { "print", { msg = src[1], mode = 0 } },
	[4] = { "wait", { time = 0.5 } },
	[5] = { "input", { type = 1 }, { [1] = { text = src[2], name = "true-3" }}},
}
return script